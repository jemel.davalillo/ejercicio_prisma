const express = require('express');
const cors = require('cors');
const app = express();

require('dotenv').config();

app.use( cors() );

//middlewares
app.use( express.json() );


app.use( express.static('public') );
app.listen(process.env.PORT_SYSTEM, () => {
    console.log(`Corriendo en el puerto ${process.env.PORT_SYSTEM}`);
});

//Se definen las rutas
app.use("/empresas", require('./routes/empresa'));
app.use("/usuarios", require('./routes/usuarios'));
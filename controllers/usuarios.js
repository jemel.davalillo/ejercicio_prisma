const response = require('express');
const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();


const getUsers = async ( req, res = response ) => {
    try {
        //Con el Promise.all calculo todas las variables en un solo await
        const [users, total_users] = await Promise.all([
            prisma.usuario.findMany({
                include: {
                    empresa: {
                        select: {
                            glosa_empresa: true
                        }
                    }
                }
            }),//users

            prisma.usuario.count()//total_users
        ]);

        return res.status(200).json({
            ok: true,
            users,
            total_users
        });
    } catch (error) {
        return res.status(400).json({
            ok: false,
            error
        });
    }
};


const createUser = async( req, res = response ) => {
    try {
        const user = await prisma.usuario.create({
            data: req.body
        });

        return res.status(200).json({
            ok: true,
            user
        });
    } catch (error) {
        return res.status(400).json({
            ok:false,
            error
        });
    }
};


const findUser = async( req, res = response ) => {
    const user_id = req.params.id_usuario;
    
    try {
        const user = await prisma.usuario.findUnique({
            where: {
                id_usuario: parseInt(user_id)
            },
            include: {
                empresa: {
                    select: {
                        glosa_empresa: true
                    }
                }
            }
        });
        
        return res.status(200).json({
            ok: true,
            user
        });
    } catch (error) {
        return res.status(400).json({
            ok: false,
            error
        });
    }
    
};


const updateUser = async( req, res = response ) => {
    const user_id = req.params.id_usuario;

    try {
        const user = await prisma.usuario.update({
            where: {
                id_usuario: user_id
            },
            data: req.body
        });

        return res.status(200).json({
            ok: true,
            user
        });
    } catch (error) {
        return res.status(400).json({
            ok: false,
            error
        });
    }
};

module.exports = {
    getUsers,
    createUser,
    findUser,
    updateUser
}
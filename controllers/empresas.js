const response = require('express');
const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();

const getEmpresas = async (req, res = response) => {
    const empresa = await prisma.empresa.findMany();
    const total_empresas = await prisma.empresa.count();

    //Respuesta de la funcion
    return res.status(200).json({
        ok: true,
        empresa,
        total_empresas
    });
};

const crearEmpresa = async(req, res = response) => {
    const { glosa_empresa } = req.body;//Se obtiene la data de la petición

    const empresa = await prisma.empresa.create({
        data: {
            glosa_empresa,
        }
    });

    return res.status(200).json({
        ok: true,
        empresa
    });
};


module.exports = {
    getEmpresas,
    crearEmpresa
}
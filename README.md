# ejercicio_prisma

## Para ver los cambios en tiempo real, es necesario instalar nodemon
``` 
    npm i -g nodemon 
```
Luego, para iniciar correr el sistema, se ejecuta el comando `npm run dev` 

## Iniciar prisma en caso de que no exista la carpeta prisma:
```
npx prisma init
```

Al iniciar prisma por primera vez, se debe configurar la conexión a la BBDD.
Prisma soporta conexión con los siguientes motores de BBDD
#### postgresql, mysql, sqlite, sqlserver, mongodb

La configuración del motor de BBDD se realiza en la variable provider del objeto datasource dentro del archivo prisma/schema.prisma

La cadena de conexión a la BBDD se configura en la variable de entorno DATABASE_URL dentro del archivo .env

-----------------------------

Para crear los modelos de una BBDD existente, se necesita ejecutar el comando de prisma desde la consola:
```
    npx prisma db pull
    npx prisma generate
```
Este comando creará los modelos en el archivo schema.prisma
Luego, para usar el orm de prisma, debe importarse el paquete en el controlador en el que se necesite e instanciarlo de la siguiente manera:

```
const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
```

Una vez instanciado, se podrán usar los métodos en todas las funciones del controlador.



Con cada modificacion de la base de datos, se deben correr los comandos 
```
npx prisma db pull
npx prisma generate
```

En caso de que se desee modificar una tabla existente o crear una nueva tabla, se puede hacer mediante migraciones agregando un nuevo modelo en el archivo schema.prisma.
Despues de crear el modelo, se debe ejecutar el comando `npx prisma migrate dev -n "nombre_de_la_migracion"` (este aplicará los cambios en la BBDD) y posteriormente `npx prisma generate`

Se puede consultar la documentación de prisma [aqui](https://www.prisma.io/docs)
const { Router } = require("express");
const { body } = require("express-validator");
const router = Router();

const { validarCampos } = require("../middlewares/validar-campos");

const { getUsers, findUser, createUser, updateUser } = require("../controllers/usuarios");

router.get("/", [], getUsers);

router.get("/:id_usuario", [], findUser);

router.post("/", [
    body("nombre", "El nombre es un campo requerido").not().isEmpty(),
    body("apellido", "El apellido es un campo requerido").not().isEmpty(),
    validarCampos
], createUser);

router.put("/:id_usuario", [], updateUser);

module.exports = router;
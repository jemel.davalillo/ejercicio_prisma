const { Router } = require("express");
const { body } = require("express-validator");
const router = Router();

//librerías propias
const { validarCampos } = require("../middlewares/validar-campos");

const { getEmpresas, crearEmpresa } = require('../controllers/empresas');

router.get("/", [], getEmpresas);

router.post("/",[
    body('glosa_empresa', 'El campo glosa_empresa es requerido').not().isEmpty(),
    validarCampos
], crearEmpresa);

module.exports = router;